#!/usr/bin/env bash
cd ~
echo ".cfg" >> .gitignore
git clone --bare https://github.com/andrewwardrobe/dotfiles.git $HOME/.cfg > /dev/null 2>&1
config="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"
mkdir -p .config-backup
$config checkout > /dev/null 2>&1
res=$?
if [ $res = 0 ]; then
echo "Checked out config.";
else
    echo "Backing up pre-existing dot files.";
    $config checkout 2>&1 | egrep "\\s+\\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}
fi;
$config checkout > /dev/null
$config config status.showUntrackedFiles no
sed -i 's/%USER%/${USER}/g' .bashrc
